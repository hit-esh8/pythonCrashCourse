# -*- coding: utf-8 -*-

# You can send a copy of a list to a function like this:
# function_name(list_name[:])
# The slice notation [:] makes a copy of the list to send to the function. 
# If we didn’t want to empty the list of unprinted designs in print_models.py, 
# we could call print_models() like this:
# print_models(unprinted_designs[:], completed_models)


# Start with users that need to be verified,
# and an empty list to hold comfirmed users.
unconfirmed_users = ['alice', 'brian', 'candice']
confirmed_users = []

# Verify each usr until there are no more unconfirmed users.
# Move each verified user into the list of confirmed users.
while unconfirmed_users:
    current_user = unconfirmed_users.pop()

    print('Verifying user: ' + current_user.title())
    confirmed_users.append(current_user)
# Display all confirmed users.
print('\nThe following users have been confirmed:')
for confirmed_user in confirmed_users:
    print(confirmed_user.title())

print('\n{:#^70}\n'.format(''))

responses = {}

# Set a flag to indicate that polling is active.
polling_active = True

while polling_active:
    # Prompt for the person's name and response.
    name = raw_input('\nWhat is your name? ')
    response = raw_input('Which mountain would you like to climb someday? ')

    # Store the response in the dictionary:
    responses[name] = response

    # Find out if anyone else is going to take the poll.
    repeat = raw_input('Would you like to let another person respond? (yes/ no) ')
    if repeat == 'no':
        polling_active = False

# Polling is complete. Show the results.
print('\n--- Poll Results ---')
for name, response in responses.items():
    print(name + ' would like to climb ' + response + '.')

print('\n{:#^70}\n'.format(''))

def make_film(film_name, actor, actress=''):
    if actress:
        movie = { 'film_name': film_name.title(), 'actor': actor.title(), 'actress': actress.title()}
    else:
        movie = { 'film_name': film_name.title(), 'actor': actor.title()}
    return movie

# list of movie dictionaries
film_list = []
while True:
    print ('\nPlease neter the movie details:')
    print ('(enter \'q\' to quit)')

    movie_name = raw_input('Movie name: ')
    if movie_name == 'q':
        break

    actor_name = raw_input('Actor name: ')
    if actor_name == 'q':
        break
    actress_name = raw_input('Actress name: ')
    if actress_name == 'q':
        break

    film = make_film(movie_name, actor_name, actress_name)
    film_list.append(film)
    
for film in film_list:
    print('\n\033[4m' + film['film_name'] + '\033[0m') # Underlined
    print('Actors : ' + film['actor'] + ', ' + film['actress'])


def build_profile(first, last, **user_info):
    """ Build a dictionay containing everything we know about a user. """
    profile = {}
    profile['first_name'] = first
    profile['last_name'] = last
    for key, value in user_info.items():
        profile[key] = value
    return profile

user_profile = build_profile('albert', 'einstein',
                              location='princeton',
                              field='physics')
print(user_profile)