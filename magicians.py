magicians = ['hero', 'villian', 'joker']

def show_magicians(magicianslist):
    for name in magicianslist:
        print(name)

def make_great(magicianslist):
    for i in range(len(magicianslist)):
        magicianslist[i] = 'Great ' + magicianslist[i]
    show_magicians(magicianslist)

show_magicians(magicians)
make_great(magicians[:])
show_magicians(magicians)