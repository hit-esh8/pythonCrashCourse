from restaurant import Restaurant
class IceCreamStand(Restaurant):
    def __init__(self, restaurant_name, cuisine_type, number_served=0):
        super(IceCreamStand, self).__init__(restaurant_name, cuisine_type, number_served=0)

        self.flavors = ['chocolate', 'pista', 'kesar']

    def get_flavors(self):
        for fl in self.flavors:
            print('- ' + fl.title())


ic = IceCreamStand('rollover', 'fresh')
ic.describe_restaurant()
ic.increment_number_served(1)
ic.describe_restaurant()
ic.get_flavors()

res2 = Restaurant('indraprastha', 'indian')
res2.describe_restaurant()