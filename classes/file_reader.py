# r readonly, w writeonly, a append, r+ read & write

try:
    # real file's full content and display
    filename = 'pi_digits.txt'

    with open(filename) as f_obj:
        contents = f_obj.read()
        print(contents.rstrip()) # right strip whitespace characters
except IOError:
    print('File does not exist.')

try:
    # read file line by line
    filename = 'pi_million_digits.txt'

    with open(filename) as f_obj:
        lines = f_obj.readlines()
        pi_string = ''
        for line in lines:
            pi_string += line.strip()
    
        print(pi_string[0:52] + '...')
        print('hitesh occurs ' + str(pi_string.replace('2703', 'hitesh').count('hitesh')) + ' times')
        print(len(pi_string))
except:
    print('an error occurred')

birthday = raw_input('Enter your birthday, in the form mmddyy: ')
if birthday in pi_string:
    print('Your birthday appears in the first million digits of pi!')
else:
    print('Your birthday does not appear in the first million digits of pi.')

##############################################
# Python 2.7 has IOError not FileNotFoundError
try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError
##############################################


def count_words(filename):
    """Count the approximate number of words in a file."""
    try:
        with open(filename, 'r') as file:
            contents = file.read()
    except FileNotFoundError:
        # msg = 'Sorry, the file ' + filename + ' does not exist.'
        # print(msg)
        pass # Failing silently
    else:
        # Count the approximate number of words in the file.
        words = contents.split()
        num_words = len(words)
        print('The file ' + filename + ' has about ' + str(num_words) + ' words.')

filenames = ['alice.txt', 'dog.py', 'ice.py', 'pi_digits.txt']
for filename in filenames:
    count_words(filename)


# write to a file
filename = 'guests.txt'
guest_name = raw_input('Enter your name: ')
with open(filename, 'w') as file:
    file.write(guest_name)

# append to a file
filename = 'guest_book.txt'
guest_name = ''
guest_list = []
while guest_name != True:
    guest_name = raw_input('Please enter your name (type \'q\' to quit): ')
    if guest_name == 'q':
        break
    with open(filename, 'a') as file:
        file.write(guest_name + '\n')
    guest_list.append(guest_name)

if guest_list:
    print('The following guests have been invited:')
    for guest in guest_list:
        print(' -' + guest.title())
    