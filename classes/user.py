import datetime
class User(object):
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

        self.login_attempts = 0

        # date(year, mon, day)
        self.join_date = datetime.date(2018, 7, 23)

    def describe_user(self):
        long_title = self.first_name + ' ' + self.last_name + ' (' + str(self.join_date)  + ')'
        print(long_title.title())
    
    def greet_user(self):
        print('Welcome, ' + self.first_name)

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0

    
class Administrator(User):
    def __init__(self, first_name, last_name):
        super(Administrator, self).__init__(first_name, last_name)

        self.privileges = ['can add post', 'can delete post', 'can ban user']

    def show_privileges(self):
        for privilege in self.privileges:
            print ' -' + privilege
us = User('hitesh', 'vaswani')
us.greet_user()
us.describe_user()

print('login attempts :' + str(us.login_attempts))
us.increment_login_attempts()
us.increment_login_attempts()
print('login attempts :' + str(us.login_attempts))
us.reset_login_attempts()
print('login attempts :' + str(us.login_attempts))

ad = Administrator('athena', 'home')
ad.greet_user()
ad.describe_user()
ad.show_privileges()