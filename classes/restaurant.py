class Restaurant(object):
    def __init__(self, restaurant_name, cuisine_type, number_served=0):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type        
        self.number_served = number_served

    def describe_restaurant(self):
        print(self.restaurant_name.title() + ' - ' + self.cuisine_type.title())

        if self.number_served:
            print('Served ' + str(self.number_served) + ' customers.')
 
    def set_number_served(self, served):
        self.number_served = served

    def increment_number_served(self, served):
        self.number_served += served

    def open_restaurant(self):
        print('Welcome, ' + self.restaurant_name.title() + ' is now open.')

# res1 = Restaurant('fiveStar', 'icelandic')
# res2 = Restaurant('indraprastha', 'indian')
# res3 = Restaurant('kaapi katti', 'chats')

# res1.describe_restaurant()
# res2.describe_restaurant()
# res3.describe_restaurant()

# res3.number_served = 99

# res3.describe_restaurant()

# res2.set_number_served(43)
# res2.describe_restaurant()

# res3.increment_number_served(1)
# res3.describe_restaurant()
