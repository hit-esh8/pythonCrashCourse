from random import randint
class Die(object):
    """Class that represents a Die model."""
    def __init__(self, sides=6):
        self.sides = sides

    def roll_die(self):
        x = randint(1, self.sides)
        print (x)

# 6 sided die
d6 = Die(6)
# roll 10 times
for i in range(1, 11):
    d6.roll_die()