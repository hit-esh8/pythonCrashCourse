"""OrderedDict combines the main benefit of lists (retaining your original order)
   with the main feature of dictionaries (connecting pieces of information)."""
from collections import OrderedDict

favorite_languages = OrderedDict()

favorite_languages['jen'] = 'python'
favorite_languages['sarah'] = 'c'
favorite_languages['edward'] = 'ruby'
favorite_languages['phil'] = 'python'

for name, language in favorite_languages.items():
    print(name.title() + '\'s favorite language is ' +
        language.title() + '.')