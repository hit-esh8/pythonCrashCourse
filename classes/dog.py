# Creating classes
# Python 2.7 Dog(object)
# Python 3   Dog()
class Dog(object):
    """A simple attempt to model a dog."""

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def sit(self):
        """Simulate a dog sitting in response to a command."""
        print(self.name.title() + ' is now sitting.')

    def roll_over(self):
        """Simulate rolling over in response to a command."""
        print(self.name.title() + ' rolled over!')

    # Modifying an attribute's value through a method.
    def update_age(self, age):
        """
        Set the dog's age to the given value
        Reject the change if it attempts to roll the age back.
        """
        if age >= self.age:
            self.age = age
        else:
            print('You can\'t roll back age.')

    def increment_age(self, age):
        """Add the given years to the age."""
        self.age += age

my_dog = Dog('willie', 6)
your_dog = Dog('lucy', 3)
print (type(my_dog))
print ('My dog\'s name is ' + my_dog.name.title() + '.')
print ('My dog is ' + str(my_dog.age) + ' years old.')
my_dog.sit()
print ('Your dog\'s name is ' + your_dog.name.title() + '.')
print ('Your dog is ' + str(your_dog.age) + ' years old.')
your_dog.roll_over()

# Modify an attribute's value directly.
my_dog.age = 2
print ('age: ' + str(my_dog.age))

my_dog.update_age(8)
my_dog.update_age(7)
my_dog.increment_age(2)
print ('age: ' + str(my_dog.age))
