from __future__ import print_function
class Stock(object):
    def __init__(self, stock_id, name, model, sizes, price):
        self.stock_id = stock_id
        self.name = name
        self.model = model
        self.sizes = sizes
        self.price = price

    def describe_stock(self):
        print(str(self.stock_id) + ' ' + self.name.title() + ' ' + self.model + ' Rs.' + str(self.price))
        for item in self.sizes:
            print('id: ' + str(item['id']) + ' size: ' + str(item['size']) + ' qty: ' + str(item['qty']))
        
        first_col = 10
        next_col = 4
        sizeli = []
        colorli = []
        for item in self.sizes:
            sizeli.append(item['size'])
            colorli.append(item['color'])

        sizelist = list(sorted(set(sizeli)))
        colorlist = list(sorted(set(colorli)))

        print()
        print('{:>10}'.format(''),end="")
        for item in sizelist:
            print('{:>4}'.format('') + str(item),end="")
        print()

        for i in range(len(colorlist)):
            c_color = colorlist[i]
            print('{:>4}'.format('') + colorlist[i],end="")

            for item in sizelist:
                qty = 0
                for item2 in self.sizes:
                    if item == item2['size'] and c_color == item2['color']:
                        qty = item2['qty']
                if int(qty) > 0:
                    print('{:>4}'.format('') + '0' + str(qty),end="")
                elif int(qty) == 0:
                    print('{:>4}'.format('') + '--',end="")
                elif int(qty) < 1:
                    print('{:>4}'.format('') + str(qty),end="")
            print()
        print()














    
sz = [{'id': 1, 'color': 'black', 'size': 7, 'qty': 1},
      {'id': 2, 'color': 'black', 'size': 8, 'qty': 2}, 
      {'id': 5, 'color': 'brown', 'size': 10, 'qty': -1}]
st = Stock(3, 'rexwood', '271', sz, 450)

st.describe_stock()