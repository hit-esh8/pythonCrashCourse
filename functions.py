# Default arguments
def get_formatted_name(first_name, last_name, middle_name=''):
    # Return a full name, neatly formatted.
    if middle_name:
        full_name = first_name + ' ' + middle_name + ' ' + last_name
    else:
        full_name = first_name + ' ' + last_name
    return full_name.title()

musician = get_formatted_name('jimi', 'hendrix')
print (musician)

musician = get_formatted_name('john', 'hooker', 'lee')
print (musician)

print('\n{:#^70}\n'.format(''))

# Return a dictionary of information about a person.
def build_person(first_name, last_name):
    person = {'first': first_name, 'last': last_name}
    return person

musician = build_person('jimi', 'hendrix')
print (musician)

# Passing keyword arguments as name-value pair
musician = build_person(first_name='john', last_name='hooker')
print(musician)

while True:
    print ('\nPlease tell me your name:')
    print ('(enter \'q\' at any time to quit)')

    f_name = raw_input('First name: ')
    if f_name == 'q':
        break
  
    l_name = raw_input('Last name: ')
    if l_name == 'q':
        break

    formatted_name = get_formatted_name(f_name, l_name)
    print ('\n Hello, ' + formatted_name + '!')  

# Arbitrary number of arguments
# single asterisk tuple
# double asterisk dictionary
# Makes an empty tuple and packs whatever valu(s) it receives
# into this tuple.
def make_pizza(*toppings):
    # Print the list of toppings that have been requested.
    print ('\nMaking a pizza with the following toppings:')
    for topping in toppings:
        print ('- ' + topping)

make_pizza('pepperoni')
make_pizza('mushrooms', 'green peppers', 'extra cheese')
